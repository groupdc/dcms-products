<?php

namespace Dcms\Productsintraco\Models;

use Dcms\Core\Models\EloquentDefaults;

class ProductsInformation extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "products_to_products_information";
    protected $primaryKey = 'product_id';
}
