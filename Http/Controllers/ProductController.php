<?php

namespace Dcms\Productsintraco\Http\Controllers;

use Dcweb\Dcms\Controllers\BaseController;

use Dcms\Productsintraco\Models\Product;
use Dcms\Productsintraco\Models\Informationintraco as Information;
use Dcms\Productsintraco\Models\ProductsInformation;
use Dcms\Products\Models\Attachment;
use Dcms\Products\Models\Price;
use Dcms\Products\Models\Category;

use Dcms\Productsintraco\Models\Label;
use Dcms\Productsintraco\Models\Labeldetail;

use View;
use Input;
use Session;
use Validator;
use Redirect;
use DB;
use Datatables;
use Auth;

class ProductController extends \Dcms\Products\Http\Controllers\ProductController
{
    function __construct()
    {
        parent::__construct(); //sets the default columnnames, we can extend these doing an array_merge

        $this->productColumNames = array_merge($this->productColumNames, array());
        $this->productColumnNamesDefaults = array_merge($this->productColumnNamesDefaults, array('new' => '0'));

        $this->informationColumNames = array_merge($this->informationColumNames, array());
        $this->extendgeneralTemplate = 'dcmsproductsintraco::products/templates/pages';
    }

    public static function getProductInformationByLanguage($language_id = null)
    {
        return Information::where("language_id", "=", $language_id)->lists("title", "id");
    }

    //return the model to fill the form
    public function getExtendedModel($id = null)
    {
        if (is_null($id)) {
            return array(   'Labels'=>DB::connection("project")->select('
														SELECT  products_labels.sort_id,products_labels.id, 0 as checked, concat(\'<img src="\',products_labels_language.image,\'" />\'  )/* group_concat( distinct concat(\'<img src="\',products_labels_language.image,\'" />\'  ) SEPARATOR  \'\' ) */ as image
														FROM products_labels
														INNER JOIN products_labels_language on products_labels_language.label_id = products_labels.id
														GROUP BY (products_labels.id)
                                                        ORDER BY products_labels.sort_id'),
                            'pageOptionValuesSelected' => array()
                        );
        } else {
            return array(   'Labels'=>DB::connection("project")->select('
                                        (SELECT  products_labels.sort_id,products_labels.id, 1 as checked, concat(\'<img src="\',products_labels_language.image,\'" />\'  ) /* group_concat( distinct concat(\'<img src="\',products_labels_language.image,\'" />\'  ) SEPARATOR  \'\' )*/  as image
                                            FROM products_labels
                                            INNER JOIN products_labels_language on products_labels_language.label_id = products_labels.id
                                            INNER JOIN products_to_products_labels ON products_to_products_labels.label_id = products_labels.id
                                            WHERE products_to_products_labels.product_id = ?
                                            GROUP BY (products_labels.id))
                                        UNION
                                        (SELECT  products_labels.sort_id,products_labels.id, 0 as checked, concat(\'<img src="\',products_labels_language.image,\'" />\'  ) /*group_concat( distinct concat(\'<img src="\',products_labels_language.image,\'" />\'  )  SEPARATOR  \'\')*/ as image
                                            FROM products_labels
                                            INNER JOIN products_labels_language on products_labels_language.label_id = products_labels.id
                                            WHERE label_id NOT IN (SELECT label_id FROM products_to_products_labels WHERE product_id = ?)
                                            GROUP BY (products_labels.id))
                                            ORDER BY 1', array($id,$id)),
                            'pageOptionValuesSelected' => $this->setPageOptionValues($this->getSelectedPages($id)));
        }
    }

    public function getSelectedPages($productid = null)
    {
        //Fetch the first and best information group_id
        //$Product = Product::with('breinformation')->find($productid);
        $Product = ProductsInformation::where('product_id', '=', $productid)->get(['product_information_id'])->groupBy('product_information_id')->keys();
        $Information = Information::whereIn('id', $Product)->get();

        //$Information = $Product->information;
        $information_group_id = null;
        $information_id = null;
        $aInformation_id = array();
        if (count($Information) > 0) {
            foreach ($Information as $I) {
                if (!is_null($I->information_group_id)) {
                    $information_id = $I->id;
                    $aInformation_id[] = $I->id;
                    $information_group_id = $I->information_group_id;
                    //break;
                }
            }
        }

        return DB::connection("project")->select('  SELECT information_group_id, page_id
													FROM pages_to_products_information_group
													WHERE
                                                    information_group_id = ?
                                                    AND
                                                    information_id IN (' . implode(',', $aInformation_id) . ')', array($information_group_id));
    }

    public function setPageOptionValues($objselected_pages)
    {
        $pageOptionValuesSelected = array();
        if (count($objselected_pages) > 0) {
            foreach ($objselected_pages as $obj) {
                $pageOptionValuesSelected[$obj->page_id] = $obj->page_id;
            }
        }
        return $pageOptionValuesSelected;
    }

	public function saveProductLabel($ProductObject)
	{
		$input = Input::get();
		$Product = \Dcms\Productsintraco\Models\Product::find($ProductObject->id);
		$Product->labels()->detach();

		if (isset($input["label"]) && count($input["label"])>0) {
			foreach ($input["label"] as $labelid) {
				$Product->labels()->attach($labelid);
			}
		}
	}

    public function saveProductToPage($ProductObject)
    {
        $input = Input::get();

        $Product = \Dcms\Productsintraco\Models\Product::find($ProductObject->id);

        $Information = $Product->information;

        if (count($Information) > 0) {
            foreach ($Information as $I) {
                if (!is_null($I->information_group_id)) {
                    $I->pages()->detach(); //->wherePivot('information_group_id','=',$I->information_group_id); //

                    if (isset($input["page_id"]) && count($input["page_id"]) > 0) {
                        foreach ($input["page_id"] as $language_id => $thepages) {
                            foreach ($thepages as $p => $page_id) {
                                $I->pages()->attach($page_id, array('information_group_id' => $I->information_group_id));
                            }
                        }
                        break;
                    }
                }
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ($this->validateProductForm() === true) {
            $Product = $this->saveProductProperties();
            $this->saveProductInformation($Product);
            $this->saveProductPrice($Product);
            $this->saveProductAttachments($Product);
			$this->saveProductLabel($Product);
            $this->saveProductToPage($Product);

            // redirect
            Session::flash('message', 'Successfully created Product!');
            return Redirect::to('admin/products');
        } else return $this->validateProductForm();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        if (parent::validateProductForm() === true) {
            $Product = $this->saveProductProperties($id);
            $this->saveProductInformation($Product);
            $this->saveProductPrice($Product);
            $this->saveProductAttachments($Product);
			$this->saveProductLabel($Product);
            $this->saveProductToPage($Product);

            // redirect
            Session::flash('message', 'Successfully updated Product!');
            return Redirect::to('admin/products');
        } else return $this->validateProductForm();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        parent::destroy($id);

        // redirect
        Session::flash('message', 'Successfully deleted the Product!');
        return Redirect::to('admin/products');
    }


    /**
     * get the data for DataTable JS plugin.
     *
     * @return Response
     */
    public function getDatatable()
    {
        return Datatables::queryBuilder(
            DB::connection("project")->table("products")->select(
                "products.id",
                "products.online",
                "products.code",
                "products.eancode",
                "products_to_products_information.product_information_id as info_id",
                "title",
                DB::raw('CONCAT(volume, " ", volume_unit) AS volume'),
                (DB::connection("project")->raw("concat(\"<img src='/packages/dcms/core/images/flag-\", lcase(settings.country),\".png' > \") as country")),
                "selling.id as country_id"
            )
                ->leftJoin('products_to_products_information', 'products.id', '=', 'products_to_products_information.product_id')
                ->leftJoin('products_information', 'products_information.id', '=', 'products_to_products_information.product_information_id')
                ->leftJoin('products_volume_units', 'products.volume_unit_id', '=', 'products_volume_units.id')
                ->leftJoin('languages', 'products_information.language_id', '=', 'languages.id')
                ->leftJoin('products_price', function ($join) {
                    $join->on('products_price.product_id', '=', 'products.id');
                    $join->on('products_price.country_id', '=', 'languages.country_id');
                })
                ->leftJoin('countries as selling', 'products_price.country_id', '=', 'selling.id')
                ->leftJoin('countries as settings', 'languages.country_id', '=', 'settings.id')
        )
            ->addColumn('edit', function ($model) {
                return '<form method="POST" action="/admin/products/' . (isset($model->info_id) ? $model->info_id : $model->id) . '" accept-charset="UTF-8" class="pull-right"> <input name="_token" type="hidden" value="' . csrf_token() . '"> <input name="_method" type="hidden" value="DELETE">
                						<input type="hidden" name="table" value="' . (isset($model->info_id) ? "information" : "product") . '"/>
                						<input type="hidden" name="product_id" value="' . $model->id . '"/>
        								<a class="btn btn-xs btn-default" href="/admin/products/' . $model->id . '/edit"><i class="fa fa-pencil"></i></a>
        								<!--<a class="btn btn-xs btn-default" href="/admin/products/' . $model->id . '/copy/' . $model->country_id . '"><i class="fa fa-copy"></i></a>-->
        								<button class="btn btn-xs btn-default" type="submit" value="Delete this product category" onclick="if(!confirm(\'Are you sure to delete this item?\')){return false;};"><i class="fa fa-trash-o"></i></button>
			                         </form>';
            })
            ->rawColumns(['country', 'edit'])
            ->make(true);
    }
}
